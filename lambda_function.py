import requests
import html

def lambda_handler(event):
    """Main method. Called from AWS Lex

    Keyword arguments:
    event -- the Lex event including intent details and slots
    """

    intent_name = event['currentIntent']['name']
    
    if intent_name == 'HelloIntent':
        return getTrivia()
    elif intent_name == 'GetAnswerIntent':
        return checkAnswer(event)
        
def checkAnswer(event):
    """Checks if the provided answer is correct.

    Keyword arguments:
    event -- the Lex event
    """
    providedAnswer = event['currentIntent']['slots']['ProvidedAnswer']
    correctAnswer = event['currentIntent']['slots']['CorrectAnswer']
    if providedAnswer == correctAnswer:
        msg = 'Correct answer!'
    else:
        msg = 'The answer '+providedAnswer+' is incorrect. The correct answer is '+ correctAnswer
    return {
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': 'Fulfilled',
            'message': {
                'contentType': 'PlainText',
                'content': msg
                }
            }
        }

def getTrivia():
    """Makes an HTTP GET request to the Open Trivia database
    """
    r = requests.get("https://opentdb.com/api.php?amount=1&type=multiple")
    return createReturn(r.json()['results'][0])


def createReturn(value):
    """Creates an AWS Lex DialogAction object from a trivia question.

    Keyword arguments:
    value -- the trivia question
    """
    return {
        'dialogAction': {
            'type': 'ElicitSlot',
            'message': {
                'contentType': 'PlainText',
                'content': html.unescape(value['question'])
            },
            'intentName': 'GetAnswerIntent',
            'slots': {
                'Question': html.unescape(value['question']),
                'CorrectAnswer': value['correct_answer'],
                'AnswerOne': value['correct_answer'],
                'AnswerTwo': value['incorrect_answers'][0],
                'AnswerThree': value['incorrect_answers'][1],
                'AnswerFour': value['incorrect_answers'][2],
                'ProvidedAnswer': ''
            },
            'slotToElicit': 'ProvidedAnswer',
            'responseCard': {
                'version': 1,
                'contentType': 'application/vnd.amazonaws.card.generic',
                'genericAttachments': [{
                    'buttons': [{
                            'text': value['correct_answer'],
                            'value': value['correct_answer']
                        },
                        {
                            'text': value['incorrect_answers'][0],
                            'value': value['incorrect_answers'][0]
                        },
                        {
                            'text': value['incorrect_answers'][1],
                            'value': value['incorrect_answers'][1]
                        },
                        {
                            'text': value['incorrect_answers'][2],
                            'value': value['incorrect_answers'][2]
                        },
                    ]
                }]
            }
        }
    }
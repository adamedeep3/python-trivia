# Chatbot Development Guide

## Login
TODO

## Cloud9 - Lambda Development
1. Click the AWS logo in the top left hand corner to go back to the main screen.

![alt-text][lambda1]
1. At the main screen, type "Cloud9"
![alt-text][lambda2]
1. On the Cloud9 front page, click "Create Environment"
![alt-text][lambda3]
1. For the name enter your first name followed by the first letter of your last name. For example "AdamE". Then click "Next Step".
1. On the next page leave all settings as they are and click "Next step". On the final page click "Create Environment"
1. Cloud9 will now start, this may take a few minutes.
1. The Cloud9 IDE should now look like this:
![alt-text][lambda4]
1. Before we start we have to change some of the settings in Cloud9 so it uses Python3. Click on the cog icon in the top right hand corner.
![alt-text][lambda5]
1. On the settings page, scroll down until you find "Python Settings" and change "Python 2" to "Python 3"
![alt-text][lambda6]
1. Close the settings page by click on the X on the tab
![alt-text][lambda7]
1. TODO DOWNLOAD CODE TEMPLATE
1. At the bottom of the Cloud9 interface there is a terminal where you can type commands. We are going to use this to install some Python libraries. Type the following commands into the terminal:
   - `sudo yum update`
   - `cd <project_folder>`
   - `sudo yum install python34-setuptools`
   - `sudo easy_install-3.4 pip`
   - `pip install requests -t ./`
   - `pip-3.6 install ikp3db -t .`
1. These commands will install some Python utilities as well as an HTTP request library. It will also install a debugger allowing you to debug your program.

[lambda1]: screenshots/image6.png
[lambda2]: screenshots/image12.png
[lambda3]: screenshots/image3.png
[lambda4]: screenshots/image7.png
[lambda5]: screenshots/image11.png
[lambda6]: screenshots/image2.png

## Amazon Lex
1. Click the AWS logo in the top left hand corner to go back to the main screen.
![alt-text][lex1]
1. Click "Create"
![alt-text][lex2]
1. On the next page click "Custom Bot" and fill in the following details
  a. Bot name - Your first name and first letter of last name and "Bot". For example "AdamEBot"
  b. Language - English (U.S)
  c. Output voice - None. This is only a text based application
  d. Session timeout - 5 minutes
  e. IAM Role - Leave as is
  f. COPPA - Yes
1. Click "Create"
![alt-text][lex3]
1. Welcome to your bot! The first thing we will do is to create an Intent. An Intent is used for a particular part of your bot. You'll be making a multi-intent bot eventually but we'll start with one.
1. Click "Create Intent" and then "Create Intent" again.
1. Enter a name for your intent. Put your first name and first letter of last name at the start of the intent name. e.g. "AdamEHelloIntent"
1. A page will appear with multiple parts.
  a. Utterances - These are the phrases that you would say to your Chatbot to kick it off
  b. Slots - These are a bit like parameters for a function. They store data the user has provided.
  c. Confirmation prompt - Whether you want an "Are you sure?" prompt at the end of the intent
  d. Fulfillment - How the intent will be fulfilled. We will be using AWS Lambda which are small pieces of code.
1. For the utterance, put in "I would like a trivia question".
1. For the fulfillment, select "AWS Lambda Function". Select the lambda function with your username and select "Latest" in the version box.
![alt-text][lex4]
1. Click "Save Intent" at the bottom of the page.
1. Click the "+" button on the Intents section to create a new intent.
  a. This intent will be to capture the user's answer to the trivia question
![alt-text][lex5]
1. Click "Create Intent" again and call the intent "GetAnswerIntent" with your First name and first letter of your last name at the start (e.g. AdamEGetAnswerIntent). This intent will contain slots for the question and the multiple choice answers.
1. In the slots section make it match the image below
![alt-text][lex6]
1. For the fulfillment, select "AWS Lambda Function". Select the lambda function with your username and select "Latest" in the version box.

[lex1]: screenshots/image8.png
[lex2]: screenshots/image13.png
[lex3]: screenshots/image9.png
[lex4]: screenshots/image1.png
[lex5]: screenshots/image14.png
[lex6]: screenshots/image5.png